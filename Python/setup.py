from setuptools import setup, Extension

setup(
    name='Avoid',
    version='0.0.1',
    author='Rano Veder',
    description='Python client for the UE4 avoid benchmark simulator',
    long_description='',
    zip_safe=False,
)